(require 'org-habit)

;; Do not dim blocked tasks
(setq org-agenda-dim-blocked-tasks nil)

;; Clean up the block agenda view
(setq org-agenda-compact-blocks t)
(setq org-agenda-remove-tags t)

;; Show more than just the local context from the egenda
(setq org-show-context-detail
 `((agenda . ancestors)
   (bookmark-jump . lineage)
   (isearch . lineage)
   (default . ancestors)))

;; Custom priorities
(setq org-highest-priority ?A)
(setq org-lowest-priority ?F)
(setq org-default-priority ?C)

;; Custom agenda command definitions
;; a great tutorial is found at: http://orgmode.org/worg/org-tutorials/org-custom-agenda-commands.html
(setq org-agenda-custom-commands
  '(("N" "Notes" tags "NOTE"
     ((org-agenda-overriding-header "Notes")
      (org-tags-match-list-sublevels t)))
    ("h" "Habits" tags-todo "STYLE=\"habit\""
     ((org-agenda-overriding-header "Habits")
      (org-agenda-sorting-strategy
       '(todo-state-down effort-up category-keep))))
    ("r" "Refile" tags "REFILE"
     ((org-agenda-overriding-header "Tasks to Refile")
      (org-tags-match-list-sublevels t)))
    ("p" "Project Agenda" 
     ((tags "+SPRINT_TARGET/-TODO-NEXT-DONE-CANCELLED"
	    ((org-agenda-overriding-header "Hanging targets (+TARGET without TODO)--------------------------")
	     (org-agenda-sorting-strategy '(category-keep))
	     (org-tags-match-list-sublevels nil)  ))
      (tags "SPRINT_NOTE"
	    ((org-agenda-overriding-header "Sprint Notes")
	     (org-tags-match-list-sublevels nil)))
      (tags "+THIS_SPRINT+SPRINT_TARGET"
	    ((org-agenda-overriding-header "Sprint targets")
	     (org-agenda-sorting-strategy '(category-keep))
	     (org-tags-match-list-sublevels nil)  ))
      (tags "THIS_SPRINT+FOCUS"
	    ((org-agenda-overriding-header "Current Sprint Focus Projects")
	     (org-tags-match-list-sublevels nil)))
      (tags "THIS_SPRINT-FOCUS"
	    ((org-agenda-overriding-header "Current Sprint Touch Projects")
	     (org-tags-match-list-sublevels nil)))
      (tags "NEXT_SPRINT"
	    ((org-agenda-overriding-header "Next Sprint Projects")
	     (org-tags-match-list-sublevels nil)))
      (tags "SPRINT_BACKLOG"
	    ((org-agenda-overriding-header "Sprint Backlog Projects")
	     (org-tags-match-list-sublevels nil)))
      (tags "PROJECT-FOCUS-THIS_SPRINT-NEXT_SPRINT-SPRINT_BACKLOG-{SPRINT_*}"
	    ((org-agenda-overriding-header "Unsorted Projects")
	     (org-tags-match-list-sublevels nil)))
      (tags "ARCHIVE"
	    ((org-agenda-overriding-header "Projects or tasks ready to archive")
	     (org-tags-match-list-sublevels nil)))
      (tags "PROJECT_IDEAS"
	    ((org-agenda-overriding-header "Project Ideas")
	     (org-tags-match-list-sublevels nil)))
      nil))
    ("d" "Today's Agenda"
     ((agenda "" ((org-agenda-span 1)))
      ;; Go, Go, Go!
      (tags "GO"
            ((org-agenda-overriding-header "===========================================================================\n===================== Today's tasks (+GO) =================================\n===========================================================================")
             (org-tags-match-list-sublevels nil)
             (org-agenda-sorting-strategy '(priority-down))
	     ))
      ;; Todos in decreasing order (next, target, waiting)
      (tags-todo "+THIS_SPRINT-GO+SPRINT_TARGET/!WAITING"
                 ((org-agenda-overriding-header "---------------------------------------------------------------------------\nWaiting (+TARGET TODO:WAITING)------------------------------------")
                  (org-agenda-sorting-strategy '(priority-down))))
      (tags-todo "+THIS_SPRINT-GO+SPRINT_TARGET/!NEXT"
                 ((org-agenda-overriding-header "---------------------------------------------------------------------------\nNext target actions (+TARGET TODO:NEXT)------------------------------------")
                  (org-agenda-sorting-strategy '(priority-down))))
      (tags-todo "+THIS_SPRINT-GO+SPRINT_TARGET-DAILY/!-DONE-NEXT"
                 ((org-agenda-overriding-header "----------------------------------------------------------------------------\nSprint Targets (+TARGET TODO:-DONE-NEXT)------------------------------------")
                  (org-tags-match-list-sublevels nil)
                  (org-agenda-sorting-strategy '(priority-down))))
      (tags-todo "+THIS_SPRINT-GO/!WAITING"
                 ((org-agenda-overriding-header "Sprint waiting (TODO:WAITING)------------------------------------")
                  (org-agenda-sorting-strategy '(category-keep))))
      ;; things to sort out as you go
      (tags "REFILE-PERSONAL"
            ((org-agenda-overriding-header "Refile Tasks (+REFILE)-------------------------------------")
             (org-tags-match-list-sublevels t)))
      ;; sprint focus (that are not targets)
      (tags "THIS_SPRINT-GO+FOCUS"
            ((org-agenda-overriding-header "Sprint Focus (+FOCUS)------------------------------------")
             (org-tags-match-list-sublevels nil)))
      (tags "THIS_SPRINT-GO-SPRINT_TARGET-FOCUS"
            ((org-agenda-overriding-header "Sprint Touch Projects (-TARGET, -FOCUS)------------------------------------")
             (org-tags-match-list-sublevels nil)))
      (tags "SPRINT_NOTE"
            ((org-agenda-overriding-header "Sprint Notes")
             (org-tags-match-list-sublevels nil)))
      nil
      ))
    ("y" "Annual Sprint Details" 
     ((tags "+THIS_SPRINT+YAR"
	    ((org-agenda-overriding-header "THIS_SPRINT +YAR --------------------------")
	     (org-agenda-sorting-strategy '(category-keep))
	     (org-tags-match-list-sublevels nil)  ))
      (tags "+NEXT_SPRINT+YAR"
	    ((org-agenda-overriding-header "NEXT_SPRINT +YAR --------------------------")
	     (org-agenda-sorting-strategy '(category-keep))
	     (org-tags-match-list-sublevels nil)  ))
      (tags "+YAR-THIS_SPRINT-NEXT_SPRINT-SPRINT_01-SPRINT_02-SPRINT_03-SPRINT_04-SPRINT_05-SPRINT_06-SPRINT_07-SPRINT_08-SPRINT_09-SPRINT_10-SPRINT_11-SPRINT_12-SPRINT_13-SPRINT_14-SPRINT_15-SPRINT_16-SPRINT_17-SPRINT_18-SPRINT_19-SPRINT_20"
	    ((org-agenda-overriding-header "YAR outside of sprint")
	     (org-tags-match-list-sublevels nil) ))
      ;;--------------------------------------------------------------------------
      (tags "+YAR+SPRINT_09"
	    ((org-agenda-overriding-header "SPRINT_09 For YAR --------------------------")
	     (org-agenda-sorting-strategy '(category-keep))
	     (org-use-tag-inheritance nil)
 	     (org-tags-match-list-sublevels nil) ))
      (tags "+SPRINT_09-YAR"
	    ((org-agenda-overriding-header "-- SPRINT_09 --------------------------")
	     (org-agenda-sorting-strategy '(category-keep))
	     (org-tags-match-list-sublevels nil)  ))
      ;;--------------------------------------------------------------------------
      (tags "+YAR+SPRINT_08"
	    ((org-agenda-overriding-header "SPRINT_08 For YAR --------------------------")
	     (org-agenda-sorting-strategy '(category-keep))
	     (org-use-tag-inheritance nil)
 	     (org-tags-match-list-sublevels nil) ))
      (tags "+SPRINT_08-YAR"
	    ((org-agenda-overriding-header "-- SPRINT_08 --------------------------")
	     (org-agenda-sorting-strategy '(category-keep))
	     (org-tags-match-list-sublevels nil)  ))
      ;;--------------------------------------------------------------------------
      (tags "+YAR+SPRINT_07"
	    ((org-agenda-overriding-header "SPRINT_07 For YAR --------------------------")
	     (org-agenda-sorting-strategy '(category-keep))
	     (org-use-tag-inheritance nil)
 	     (org-tags-match-list-sublevels nil) ))
      (tags "+SPRINT_07-YAR"
	    ((org-agenda-overriding-header "-- SPRINT_07 --------------------------")
	     (org-agenda-sorting-strategy '(category-keep))
	     (org-tags-match-list-sublevels nil)  ))
      ;;--------------------------------------------------------------------------
      (tags "+YAR+SPRINT_06"
	    ((org-agenda-overriding-header "SPRINT_06 For YAR --------------------------")
	     (org-agenda-sorting-strategy '(category-keep))
	     (org-use-tag-inheritance nil)
 	     (org-tags-match-list-sublevels nil) ))
      (tags "+SPRINT_06-YAR"
	    ((org-agenda-overriding-header "-- SPRINT_06 --------------------------")
	     (org-agenda-sorting-strategy '(category-keep))
	     (org-tags-match-list-sublevels nil)  ))
      ;;--------------------------------------------------------------------------
      (tags "+YAR+SPRINT_05"
	    ((org-agenda-overriding-header "SPRINT_05 For YAR --------------------------")
	     (org-agenda-sorting-strategy '(category-keep))
	     (org-use-tag-inheritance nil)
 	     (org-tags-match-list-sublevels nil) ))
      (tags "+SPRINT_05-YAR"
	    ((org-agenda-overriding-header "-- SPRINT_05 --------------------------")
	     (org-agenda-sorting-strategy '(category-keep))
	     (org-tags-match-list-sublevels nil)  ))
      ;;--------------------------------------------------------------------------
      (tags "+YAR+SPRINT_04"
	    ((org-agenda-overriding-header "SPRINT_04 For YAR --------------------------")
	     (org-agenda-sorting-strategy '(category-keep))
	     (org-use-tag-inheritance nil)
 	     (org-tags-match-list-sublevels nil) ))
      (tags "+SPRINT_04-YAR"
	    ((org-agenda-overriding-header "-- SPRINT_04 --------------------------")
	     (org-agenda-sorting-strategy '(category-keep))
	     (org-tags-match-list-sublevels nil)  ))
      ;;--------------------------------------------------------------------------
      (tags "+YAR+SPRINT_03"
	    ((org-agenda-overriding-header "SPRINT_03 For YAR --------------------------")
	     (org-agenda-sorting-strategy '(category-keep))
	     (org-use-tag-inheritance nil)
 	     (org-tags-match-list-sublevels nil) ))
      (tags "+SPRINT_03-YAR"
	    ((org-agenda-overriding-header "-- SPRINT_03 --------------------------")
	     (org-agenda-sorting-strategy '(category-keep))
	     (org-tags-match-list-sublevels nil)  ))
      ;;--------------------------------------------------------------------------
      (tags "+YAR+SPRINT_02"
	    ((org-agenda-overriding-header "SPRINT_02 For YAR --------------------------")
	     (org-agenda-sorting-strategy '(category-keep))
	     (org-use-tag-inheritance nil)
 	     (org-tags-match-list-sublevels nil) ))
      (tags "+SPRINT_02-YAR"
	    ((org-agenda-overriding-header "-- SPRINT_02 --------------------------")
	     (org-agenda-sorting-strategy '(category-keep))
	     (org-tags-match-list-sublevels nil)  ))
      ;;--------------------------------------------------------------------------
      (tags "+YAR+SPRINT_01"
	    ((org-agenda-overriding-header "SPRINT_01 For YAR --------------------------")
	     (org-agenda-sorting-strategy '(category-keep))
	     (org-use-tag-inheritance nil)
 	     (org-tags-match-list-sublevels nil) ))
      (tags "+SPRINT_01-YAR"
	    ((org-agenda-overriding-header "-- SPRINT_01 --------------------------")
	     (org-agenda-sorting-strategy '(category-keep))
	     (org-tags-match-list-sublevels nil)  ))
      nil))
    ))




