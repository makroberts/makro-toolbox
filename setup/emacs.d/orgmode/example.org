
* Details
[2017-10-03 Tue 08:11]
** An item with TODO state is called a task
[2017-10-03 Tue 08:11]
- These items can be added in several ways
  - Shift+Enter inserts it where the cursor is, so it is best to use at the   beginning or end of a line
*** TODO This is an example of a task
[2017-10-03 Tue 08:12]
*** DONE Another example of a task
CLOSED: [2017-10-03 Tue 08:12]
[2017-10-03 Tue 08:12]
** An item without TODO state is called a headline
[2017-10-03 Tue 08:12]
- Headlines can be added in several ways
  - a
  - b
  - c
** Sprint Structure
[2017-10-03 Tue 08:13]
*** TODO Copy the explanation here after editing it
[2017-10-03 Tue 08:16]
*** Principles include
[2017-10-03 Tue 08:19]
**** Minimize (or eliminate) effort on deferred tasks until you are ready
**** Simple "project" hierarchy (no special 'PROJECT' tag required)
- Any task is a project
- Any headline with at least one subtask is a project
- Any org-file with at least one (sub)task is a project
- Use headlines or org-files for projects that run over many sprints
- Projects that run within _this_ sprint are tasks tagged THIS_SPRINT
- Projects deferred to later are tagged NEXT_SPRINT or SPRINT_BACKLOG
**** Pick up where you left off with ease
**** View from top to bottom - old items in reverse chronology
**** Use NEXT Task to focus attention and keep projects moving
**** Separate Processing, Planning, and Progressing for Projects
**** No more advancing TODO items
**** Easily "pull up" tasks for the day, week, or sprint
**** Tag items with YAR for items you want to report
**** Tag items with SPRINT_NN (for 0 < NN <20) to track which sprint an item is done
**** Principle of "three" for all sprint strata
- Calender: Deadlines, Scheduled, and [Placeholders]
  - Place NO reminders here! Only deadlines and meetings!
- Sprints: this, next, and backlog
- Weekly: *Go (today), *Next, and *target
  - Further ranked by priorities
    - Soon: A (ASAP) or B (Before the period's end)
    - Later: C (Can wait,important -urgent) or D (Deferred/Waiting)
    - Ticklers: E (Eventually) or F (Follow up, following, or forwarded)
- Most of your effort will focus on adjusting your weekly list
  - 15 minutes planning each morning
  - 30-90 minutes planning each week
  - 2-4 hours each sprint
**** Priorities [A-F] help organize within a sprint strata

** The sprint burndown is ...
[2017-10-03 Tue 08:13]
* Headline that is used for details or other notes
[2017-10-03 Tue 08:28]
- will not 
* Project that will show up in my YAR                                   :YAR:
[2017-10-03 Tue 08:26]
- I often use these to start projects I want to list in my YAR
- I will also use these
* Project with all sub-projects due this sprint :SPRINT_TARGET:THIS_SPRINT:
[2017-10-03 Tue 08:04]
- This headline will not show up in sprint burndown but all tasks will inherit SPRINT_TARGET and THIS_SPRINT so they *will* show up in the burndown.
** TODO Subproject due this sprint
[2017-10-03 Tue 08:06]
** TODO Another subproject due this sprint
[2017-10-03 Tue 08:06]
* TODO General Task to complete THIS sprint (or long-running "ongoing" task) :SPRINT_TARGET:THIS_SPRINT:
[2017-10-03 Tue 08:06]
- This task will stay in the current burndown until 
** TODO Todo item will not show up in sprint burndown
[2017-10-03 Tue 08:13]
