#!/usr/bin/env python3

#accessed 20170712 from https://askubuntu.com/questions/751413/how-to-disable-enable-toggle-touchpad-in-a-dell-laptop

import subprocess

key = "org.gnome.desktop.peripherals.touchpad" ;val = "send-events"
curr = subprocess.check_output(["gsettings", "get", key, val]).decode("utf-8").strip()
newval = "disabled" if curr == "'enabled'" else "enabled"
subprocess.Popen(["gsettings", "set", key, val, newval])
