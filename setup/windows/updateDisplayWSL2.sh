
DISPLAY=$(awk '/nameserver / {print $2; exit}' /etc/resolv.conf 2>/dev/null):0
export DISPLAY
echo Set display to: $DISPLAY
export LIBGL_ALWAYS_INDIRECT=1
