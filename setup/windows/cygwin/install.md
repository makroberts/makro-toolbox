
# Running with Cygwin/X as the X Server on WSL1

This option only really works with WSL1 distributions.  

- Download the latest Cygwin/X Setup file from https://x.cygwin.com/
  - usually, this will be http://cygwin.com/setup-x86_64.exe
    but check the page for details

- Run the setup, answer the questions using defaults unless you know
  you need other settings.  I usually use the Virginia Tech mirror
  since I'm on the US east coast.

- During install you only need to the `xinit` package unless you
  know you will need additional programs.

- To use cygwin as an Xserver for WSL, you need to do start Xwin with some additional command parameters. This is done by editing the shortcut:
1. Click the XWin Server Icon in the start menu (I recommend pinning it to the start)
1. Select `More Options>Open File Location`
1. Right-click the XWin Server Icon and select `Properties`
1. Enter in 
```
C:\cygwin64\bin\run.exe --quote /usr/bin/bash.exe -l -c "cd; exec /usr/bin/startxwin -- -listen tcp -nowgl -dpi 100"
```

The `-dpi` option can be adjust to your screen resolution.  

Inside the WSL instance, link to the copy-Xauthority file and run this script each 
time you have trouble connecting because of permissions.

You will also need to add "export DISPLAY=:0" to your bash prompt before running any X program.
If you are using the setup from Mak's install you can run the following:
```
cd ~
ln -s git-workspace/makro-toolbox-git/setup/windows/cygwin/EXPORT_CYGWIN_DISPLAY
```
or add this to your appropriate shell configuration.



## Rebooting

I have found that rebooting daily can helps keep windows/CygX/WSL1 happier.
(And, data-at-rest is a requirement if you transport any DoD equipment.)
But this means loosing all my precious windows, which I detest because it 
greatly increases my morning startup cost.
My answer: tmux with the continuum plugin, which loads your last 
shell environment.  For details, checkout my tmux+zsh setup
at https://bitbucket.org/makro-nrl2/makro-toolbox/src/master/setup/install.md


## Notes on screen resolution, un/docking, and font size

I have noticed a few situations that create minor headaches.  

Generally, the screen resolution is fixed once you run the X Server.  
This means if you change screens or dock/undock, your window sizes can get wonky, 
especially if there is a a big difference in resolution between the display
and your laptop.
I'm sure there is some permenant way to fix this but I have just found that 
restarting an app works most of the time.
In the case of two linux applications, specifically emacs and IntelliJ,
I change the font size based on the setup.  

Sometimes when I dock or undock windows will not appear on the desktop
even though I can see them running on the taskbar.
To get them back, I use an old Win98+CygwinX trick:
1. Right click on the window that is hidden in the taskbar
1. Select `Move`
1. Hit any of the arrow keys to get the window to snap to the mouse
1. Click anywhere to drop the window
1. Resize as needed

Alternatively, sometimes windows can be "found" by moving them around
the screen using the WindowsKey+Left or WindowsKey+Right.




# Running an X Server for WSL2

WSL2 creates a separate virtual network interface meaning the X server 
may not allow connections from this "remote" virtual machine.

However, this workaround kills all X windows when the network
address changes such as when a VPN is started or stopped.
Thus, these are not usable solutions for my workflow.
So, I now use Ubuntu 16.04 on WSL1 and Ubuntu 18.04 for WSL2.
This way, I can still use my old things but I can also run
docker, which integrates nicely with WSL2.

If you don't mind the network change issue, the XForwarding
can be solved in one of two ways: tell CygwinX the WSL2's 
network address or install VcXsrv and allow arbitrary 
X windows connections.  I prefer the Cygwin/X solution because 
it allows me to control the network connection

I have more detailed notes and test scripts for solving 
this but stopped updating this document once I decided
to revert to WSL1 for dev (tmux, zsh, emacs, IntelliJ) 
and use WSL2 for docker only.
Maybe by the time I am forced to upgrade to WSL2 the issues
will be resolved.


## Using Cygwin/X 

1. Install and start the Cygwin/X as above
1. start the wsl instance and type `ipconfig
2. TBD


## Install VcXsrv

https://sourceforge.net/projects/vcxsrv/

Be sure to select allow all connections when starting this!

