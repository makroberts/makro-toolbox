
exit  #this script does not yet work but I'm keeping it for posterity!
Details about this script can be found at:

https://github.com/microsoft/WSL/issues/4619
https://github.com/microsoft/WSL/issues/4793
https://unix.stackexchange.com/questions/227889/cygwin-on-windows-cant-open-display
https://github.com/microsoft/WSL/issues/4106



WSL2_IP=$(ip address show dev eth0 | awk -F '[ /]+' '/inet / { print $3 }')
echo The WSL2 host ip is $WSL2_IP
export WSL2_IP

WINDOWS_HOST=$(awk '/nameserver / {print $2; exit}' /etc/resolv.conf 2>/dev/null)
DISPLAY=$WINDOWS_HOST:0
export DISPLAY
echo Set display to: $DISPLAY
export LIBGL_ALWAYS_INDIRECT=1

export WSLENV="$WSLENV:DISPLAY:WSL2_IP"

# Start the CygwinX server
#powershell.exe -Command '& "C:\Program Files\VcXsrv\vcxsrv.exe" -multiwindow -wgl'
powershell.exe -Command '& "C:\cygwin64\bin\run.exe" --quote /usr/bin/bash.exe -l -c \"cd; exec /usr/bin/startxwin -- -listen tcp -nowgl -dpi 100\"'

# Tell the server to let me in via xhost
#powershell.exe -Command '& "C:\Program Files\VcXsrv\xhost.exe" '+$WSL2_IP
powershell.exe -Command '& "C:\Program Files\VcXsrv\xhost.exe" '+$WSL2_IP
