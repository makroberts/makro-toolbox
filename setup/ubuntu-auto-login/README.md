
Scripts to allow a remote machine to reboot to a specific user (mroberts is the default).

To use:
 - link 50-unity-greeter.conf.remote and 50-unity-greeter.conf.org in your home directory
   (or copy them and modify as appropriate)
 - link the two shell scripts in this directory
 - create a link in your home directory from 50-unity-greeter.conf to the remote conf file
 - using sudo, back up and then link the /usr/share/lightdm/50-unity-greeter.conf to the home directory version you just created
   For example:
   ```   
      lrwxrwxrwx 1 root root  36 Sep 10 13:52 50-unity-greeter.conf -> /home/mroberts/50-unity-greeter.conf
   ```


