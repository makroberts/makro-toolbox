Installation instructions for Linux machine 
Last updated: 20190821

Install base programs and switch to zsh
=======================================
```
sudo apt install zsh emacs python3 python3-pip curl git zip unzip virtualenvwrapper
sudo pip3 install --upgrade pip
chsh 
```
Install oh-my-zsh happiness
===========================

-install oh-my-zsh:
```
sh -c "$(curl -fsSL https://raw.github.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
```

- install zplug:
```
curl -sL --proto-redir -all,https https://raw.githubusercontent.com/zplug/installer/master/installer.zsh | zsh
```

Install search and find helpers
===============================

- install fd, a faster replacement for find (
  - download the latest .deb package from the release page at
https://github.com/sharkdp/fd/releases

```
wget https://github.com/sharkdp/fd/releases/download/v8.1.1/fd-musl_8.1.1_amd64.deb

sudo dpkg -i fd-musl_8.1.1_amd64.deb
```
- older versions
wget https://github.com/sharkdp/fd/releases/download/v7.4.0/fd_7.4.0_amd64.deb .



- install the silver searcher, called ag, a faster replacement for awk:
```
sudo apt-get install silversearcher-ag
```

Add your ssh key to BitBucket, if needed
=========================================

First, determine if you have a public key:
```
ls -la ~/.ssh/
```
If you don't see a file called `id_rsa.pub` then you need to run `ssh-keygen`
and follow the prompts, but do **not** add a password or you will
need to type a password everytime git interacts with a remote server.

Now you need to copy this key to your bitbucket account.
First, get a copy of the key:
```
cat ~/.ssh/id_rsa.pub
```
and copy everything between the command prompts.

Open your bitbucket account, go to profile settings, find the SSH keys
area, and add the key with a name you can remember.



Clone this setup repository
===========================
```
cd ~
mkdir git-workspace
cd git-workspace
git clone git@bitbucket.org:makroberts/makro-toolbox.git makro-toolbox-git
```

Note the suffix of `-git` to help me distinguish local git repositories.
I don't use the extension `.git` because that is conventionally reserved
for remote, bare repositories.

Link your custom configs
=========================
```
cd ~
mv .zshrc .zshrc.bak
ln -s git-workspace/makro-toolbox-git/setup/dot-zshrc .zshrc
ln -s git-workspace/makro-toolbox-git/setup/dot-tmux.conf .tmux.conf
cd ~/.oh-my-zsh/custom
ln -s ~/git-workspace/makro-toolbox-git/setup/mak-custom.zsh
```
  
Link your theme via oh-my-zsh (but maybe fix loading via zplug..)
================================================================
```
cd ~/.oh-my-zsh/custom/themes
git clone https://github.com/denysdovhan/spaceship-prompt.git spaceship-prompt-git
ln -s spaceship-prompt-git/spaceship.zsh-theme
```

Install tmux plugins
====================
- start tmux
- run <command>-I (Ctrl-Q I) to install tmux plugins

Install exa
============
- install rust (details [here](https://www.rust-lang.org/tools/install) )
```
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
```

- grab the latest exa release and unzip from https://github.com/ogham/exa/releases
```
cd ~
mkdir exa
cd exa
wget https://github.com/ogham/exa/releases/download/v0.9.0/exa-linux-x86_64-0.9.0.zip
unzip exa-linux-x86_64-0.9.0.zip
sudo cp exa-linux-x86_64 /usr/local/bin/exa
```

Install SDKMAN and Java
=======================
```
curl -s "https://get.sdkman.io" | bash
sdk list java
sdk install java 8.0.252.j9-adpt
```

Install Umake and IDEs of interest
==================================
```
sudo add-apt-repository ppa:ubuntu-desktop/ubuntu-make 
sudo apt update
sudo apt install ubuntu-make
umake ide idea [or idea-ultimate]
ln -s .local/share/umake/ide/idea-ultimate/bin/idea.sh
   [or whatever was listed as the installation]
```

## Install IntelliJ
Open [INSTALL_IDEA.md](INSTALL_IDEA.md) and follow the instructions.




If on windows
=============

## Install WSL
  - Follow the instructions in [windows/cygwin/install.md](windows/cygwin/install.md)


## Install Powerline Fonts
  - Download the latest release from https://github.com/powerline/powerline/releases
  - unpack the zip file
  - run the install file for powershell
  - answer yes to all the program
  - For your bash shell in WSL's properties, select the font `DejaVu sans mono for powerline`


## Install PowerToys
- Download and install from https://github.com/microsoft/PowerToys/releases/

