# You can put files here to add functionality separated per file, which
# will be ignored by git.
# Files on the custom/ directory will be automatically loaded by the init
# script, in alphabetical order.

# For example: add yourself some shortcuts to projects you often work on.
#
# brainstormr=~/Projects/development/planetargon/brainstormr
# cd $brainstormr
#

hostname=`hostname`

#emacs related
alias e="TERM=xterm-256color; emacs -nw -q"
export EDITOR='TERM=xterm-256color; emacs -nw -q'

#virtualenvwrapper
export WORKON_HOME=$HOME/virtualenv
#don't set a project home as I don't expect to use this feature
#export PROJECT_HOME=$HOME/Devel
source /usr/share/virtualenvwrapper/virtualenvwrapper.sh

#alias helpers
alias aG="alias G" #useful to search for aliases (G is a zsh macro)
alias eG="env G" #useful for checking environment variables (G is a zsh macro) 

#misc aliases
alias ltr="ls -ltr"
alias largest-dirs="sudo du --max-depth=3 /* -h | sort -h"

#git
alias gblsd="git for-each-ref --sort=committerdate refs/heads/ --format='%(HEAD) %(color:yellow)%(refname:short)%(color:reset) - %(color:red)%(objectname:short)%(color:reset) - %(contents:subject) - %(authorname) (%(color:green)%(committerdate:relative)%(color:reset))'"
alias gline="git log --format='%aN' | sort -u | while read name; do echo -en "\$name\t"; git log --author="\$name" --pretty=tformat: --numstat | awk '{ add += \$1; subs += \$2; loc += \$1 - \$2 } END { printf \"added lines: %s, removed lines: %s, total lines: %s\n\", add, subs, loc }' -; done"
alias gbage='for k in `git branch -a | perl -pe '\''s/^..(.*?)( ->.*)?$/\1/'\''`; do echo -e `git show --pretty=format:"%Cgreen%ci %Cblue%cr%Creset" $k -- | head -n 1`\\t$k; done | sort'
#  the one I really want!
#  alias gbage='for k in `git branch -a | sed -e s/^..// -e 's/(detached from .*)/HEAD/'`; do echo -e `git log -1 --pretty=format:"%Cgreen%ci |%Cblue%cr |%Creset$k |%s" $k --`;done | sort | column -t -s "|"'
#  other cool ones to try
#  git for-each-ref --sort='-authordate:iso8601' --format=' %(authordate:relative)%09%(refname:short)' refs/heads
#  git for-each-ref --sort='-authordate:iso8601' --format=' %(authordate:relative)%09%(refname:short)' refs/heads
#  for branch in `git branch -r | grep -v HEAD`;do echo -e `git show --format="%ci %cr" $branch | head -n 1` \\t$branch; done | sort
alias gsf="git show --stat --oneline"
alias grvv='git remote -vv'
alias gbvv='git branch -vv'
alias gsz="git rev-list --objects --all \
| git cat-file --batch-check='%(objecttype) %(objectname) %(objectsize) %(rest)' \
| sed -n 's/^blob //p' \
| sort --numeric-sort --key=2 \
| tail -n 50 \
| cut -c 1-12,41- \
| $(command -v gnumfmt || echo numfmt) --field=2 --to=iec-i --suffix=B --padding=7 --round=nearest"

#git worktree
alias gwt="git worktree"
alias gwtls="git worktree list"
alias gwta="git worktree add"
alias gwth="git help worktree"
alias gwtp="git worktree prune"

#latex
alias ltpdf='compile-pdflatex() { pdflatex $1 && bibtex $1 && pdflatex $1 && pdflatex $1;  };compile-pdflatex'

#mongodb
alias mst='sudo service mongodb status'
alias mup='sudo service mongodb start'
alias mdn='sudo service mongodb stop'

#fix emacs funky shell characters
[[ $EMACS = t ]] && unsetopt zle

#use a more terminal/git friendly less varaint; avoids less for short output
LESS=-FRX

#windows 10 specific setup
if [ "$hostname" = "wn14mk" ]; then
    export DOCKER_HOST=tcp://localhost:2375
    export DISPLAY=:0
fi
exportCygwinDisplay=$HOME/EXPORT_CYGWIN_DISPLAY
if [[ -e $exportCygwinDisplay ]]
then
    export DISPLAY=:0
fi


# Exa <3
alias extr="exa -lmr --git --time-style=long-iso -F -T -L1"
alias extr2="exa -lmr --git --time-style=long-iso -F -T -L2"
alias extr3="exa -lmr --git --time-style=long-iso -F -T -L3"
alias extr4="exa -lmr --git --time-style=long-iso -F -T -L4"
cde() {
    cd "$1"
    extr2
}

#remove oh-my-zsh aliases that I don't want
unalias fd


