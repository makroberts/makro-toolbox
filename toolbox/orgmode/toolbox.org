

* Github
  * Travis Continuous Integration ([[https://travis-ci.org/][main site]])


* Emacs 
** Configuraiton changes to consider
*** Take a look at Steve Purcell's [[https://github.com/purcell/emacs.d][emacs setup]]
*** Font scaling
 (global-set-key (kbd "C-+") 'text-scale-increase)
 (global-set-key (kbd "C--") 'text-scale-decrease)

Or consider [[https://github.com/purcell/emacs.d/blob/master/lisp/init-fonts.el][this solution]] 
